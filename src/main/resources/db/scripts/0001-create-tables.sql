CREATE TABLE sizes(
  id SERIAL PRIMARY KEY ,
  code VARCHAR,
  sort_order INTEGER
);